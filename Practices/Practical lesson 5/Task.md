# Практическое задание 5.1

На этом практическом занятии мы познакомимся с redux архитектурой 

Результатом работы над данным практическим занятием должно стать веб приложение "Прогноз погоды"

На данный момент вы должны знать:
- Как создать простое ReactJS приложение
- Основы TypeScript

Для работы потребуется установить следующие программы:
- yarn
- create-react-app
- react-redux
- react-router-dom

Документация 
- https://redux.js.org/basics/usage-with-react
- https://github.com/reactjs/react-redux
- https://getinstance.info/articles/react/learning-react-redux/


Цель: Создать ReactJS приложение с использованием архитектуры redux

Приложение должно состоять из списка городов в который можно добавлять новые и удалять существующие

При нажатии на город должен отображаться прогноз погоды на текущий день

Для получения погодных сведений использовать API Яндекс погоды

##### a) Создайте новое приложение ReactJS

```{r, engine='bash', count_lines}
create-react-app weather
```

##### b) Создайте родительский компонент в котором будут храниться города
```
<div className="App">
    <header className="App-header">
      <h1 className="App-title">Practical lesson 5</h1>
    </header>
    <City data={this.cities}/>
</div>
```
##### с) Создайте компонент City

```
 render() {
     let stationComponents = this.props.data.map(item => {
         return <div className="City">{item.name}</div>;
     });
     return <div>{stationComponents}</div>;
 }
```

##### d) Подключение API 

Для получения погодных данных с удаленного источника используйте fetch

После получения данных используйте предоставляемое react-redux механизм для создания нового дерева


