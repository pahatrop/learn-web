import {Ivehicle} from "./ivehicle";

export class Car implements Ivehicle {
    ride() {
    }

    stop() {
    }

    color: string;
    speed: number;
    weight: number;
}
