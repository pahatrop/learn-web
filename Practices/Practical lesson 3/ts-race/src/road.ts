import { Bmw } from "./bmw";
import { Ivehicle } from "./ivehicle";

export class Road {
    vehicles: Ivehicle[] = [];

    constructor(){
        let road = new Road();
        let bmw = new Bmw();
        bmw.color = '#ff0000';
        bmw.speed = 90;
        bmw.weight = 1000;
        road.add(bmw);
        road.start();
    }

    start(){
        setInterval(() => {
            this.vehicles.forEach((item) => {
                item.ride();
            });
        }, 250);
    }

    add(t: Ivehicle){
        this.vehicles.push(t);
    }
}
