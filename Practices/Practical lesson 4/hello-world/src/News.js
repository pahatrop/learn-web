import React, { Component } from 'react';
import './News.css';

class News extends Component {

     render() {
         let stationComponents = this.props.data.map(item => {
             return <div className="NewsItem">{item.author}</div>;
         });
         return <div>{stationComponents}</div>;
    }
}

export default News;
