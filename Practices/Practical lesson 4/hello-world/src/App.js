import React, { Component } from 'react';
import './App.css';
import News from "./News";

class App extends Component {
    news = [
        {author:'Ivan', text:'Some text'},
        {author:'Abram', text:'Some text'},
        {author:'Luke', text:'Some text'},
        {author:'Jake', text:'Some text'}
    ];
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Practical lesson 4</h1>
        </header>
        <News data={this.news}/>
      </div>
    );
  }
}

export default App;
